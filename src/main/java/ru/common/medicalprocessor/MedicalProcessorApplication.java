package ru.common.medicalprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedicalProcessorApplication.class, args);
    }

}
