package ru.common.medicalprocessor.core.model.enumeration;

public enum Gender {
    MALE,
    FEMALE
}
