package ru.common.medicalprocessor.core.controller.rest.dto.request;

import lombok.Data;
import ru.common.medicalprocessor.core.model.enumeration.Gender;

import java.time.LocalDate;

@Data
public class PatientDto {

    private String name;

    private Gender gender;

    private Integer age;

    private String city;

    private String address;

    private LocalDate birthdayDt;

    private String birthPlace;

    private String registration;

    private Integer passportSeries;

    private Integer passportNumber;

    private String phoneNumber;

    private String anotherDocument;
}
