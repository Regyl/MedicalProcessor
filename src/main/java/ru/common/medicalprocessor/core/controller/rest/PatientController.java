package ru.common.medicalprocessor.core.controller.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.common.medicalprocessor.core.controller.rest.dto.request.PatientDto;
import ru.common.medicalprocessor.core.model.entity.PatientEntity;
import ru.common.medicalprocessor.core.service.PatientService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/medical/patient")
public class PatientController {

    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/")
    public List<PatientEntity> findAll() {
        return patientService.findAll();
    }

    @GetMapping("/{id}")
    public PatientEntity findById(@PathVariable UUID id) {
        return patientService.findById(id);
    }

    @DeleteMapping("/{id}")
    public PatientEntity deleteById(@PathVariable UUID id) {
        return patientService.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public PatientEntity create(@RequestBody PatientDto dto) {
        PatientEntity patient = PatientEntity.of(dto);
        return patientService.save(patient);
    }

    @PatchMapping("/{id}")
    public PatientEntity updateById(@PathVariable UUID id, @RequestBody PatientDto dto) {
        PatientEntity entity = patientService.findById(id);
        entity.update(dto);
        return patientService.save(entity);
    }
}
