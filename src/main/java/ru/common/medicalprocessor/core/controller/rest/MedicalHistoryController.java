package ru.common.medicalprocessor.core.controller.rest;

import org.springframework.web.bind.annotation.*;
import ru.common.medicalprocessor.core.controller.rest.dto.request.MedicalHistoryDto;
import ru.common.medicalprocessor.core.model.entity.MedicalHistory;
import ru.common.medicalprocessor.core.model.entity.PatientEntity;
import ru.common.medicalprocessor.core.service.MedicalHistoryService;
import ru.common.medicalprocessor.core.service.PatientService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/medical/history")
public class MedicalHistoryController {

    private final MedicalHistoryService medicalHistoryService;
    private final PatientService patientService;

    public MedicalHistoryController(MedicalHistoryService medicalHistoryService,
                                    PatientService patientService) {
        this.medicalHistoryService = medicalHistoryService;
        this.patientService = patientService;
    }

    @GetMapping("/")
    public List<MedicalHistory> findAll() {
        return medicalHistoryService.findAll();
    }

    @GetMapping("/{id}")
    public MedicalHistory findById(@PathVariable UUID id) {
        return medicalHistoryService.findById(id);
    }

    @DeleteMapping("/{id}")
    public MedicalHistory deleteById(@PathVariable UUID id) {
        return medicalHistoryService.deleteById(id);
    }

    @PostMapping("/")
    public MedicalHistory create(@RequestBody MedicalHistoryDto dto) {
        //Prepare to create entity
        PatientEntity patient = patientService.findById(dto.getPatientId());
        MedicalHistory medicalHistoryParent = dto.getMedicalHistoryParentId() == null
                ? null :medicalHistoryService.findById(dto.getMedicalHistoryParentId());

        MedicalHistory medicalHistory = MedicalHistory.of(dto, patient, medicalHistoryParent);
        return medicalHistoryService.save(medicalHistory);
    }

    @PatchMapping("/{id}")
    public MedicalHistory updateById(@PathVariable UUID id, @RequestBody MedicalHistoryDto dto) {
        PatientEntity patient = patientService.findById(dto.getPatientId());
        MedicalHistory medicalHistoryParent = dto.getMedicalHistoryParentId() == null
                ? null :medicalHistoryService.findById(dto.getMedicalHistoryParentId());

        MedicalHistory medicalHistory = medicalHistoryService.findById(id);
        medicalHistory.update(dto, patient, medicalHistoryParent);
        return medicalHistoryService.save(medicalHistory);
    }
}
