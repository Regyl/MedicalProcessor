package ru.common.medicalprocessor.core.controller.rest.dto.request;

import lombok.Data;

import java.util.UUID;

@Data
public class MedicalHistoryDto {

    private UUID patientId;

    private String docNumber;

    private String doctor;

    private String diagnosis;

    private UUID medicalHistoryParentId;
}
