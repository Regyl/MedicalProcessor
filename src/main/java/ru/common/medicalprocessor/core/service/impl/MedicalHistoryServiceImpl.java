package ru.common.medicalprocessor.core.service.impl;

import org.springframework.stereotype.Service;
import ru.common.medicalprocessor.core.model.entity.MedicalHistory;
import ru.common.medicalprocessor.core.repository.MedicalHistoryRepository;
import ru.common.medicalprocessor.core.service.MedicalHistoryService;

import java.util.List;
import java.util.UUID;

@Service
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

    private final MedicalHistoryRepository medicalHistoryRepository;

    public MedicalHistoryServiceImpl(MedicalHistoryRepository medicalHistoryRepository) {
        this.medicalHistoryRepository = medicalHistoryRepository;
    }

    @Override
    public MedicalHistory findById(UUID id) {
        return medicalHistoryRepository.findById(id)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public List<MedicalHistory> findAll() {
        return medicalHistoryRepository.findAll();
    }

    @Override
    public MedicalHistory save(MedicalHistory entity) {
        return medicalHistoryRepository.save(entity);
    }

    @Override
    public MedicalHistory deleteById(UUID id) {
        MedicalHistory medicalHistory = findById(id);
        medicalHistoryRepository.deleteById(id);
        return medicalHistory;
    }
}
