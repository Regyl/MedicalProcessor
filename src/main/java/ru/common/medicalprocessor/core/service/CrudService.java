package ru.common.medicalprocessor.core.service;

import java.util.List;
import java.util.UUID;

public interface CrudService<T> {

    T findById(UUID id);

    List<T> findAll();

    T save(T entity);

    T deleteById(UUID id);

}
