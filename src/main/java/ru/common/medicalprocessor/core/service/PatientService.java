package ru.common.medicalprocessor.core.service;

import ru.common.medicalprocessor.core.model.entity.PatientEntity;

public interface PatientService extends CrudService<PatientEntity> {

}
