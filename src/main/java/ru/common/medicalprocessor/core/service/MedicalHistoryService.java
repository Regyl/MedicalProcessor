package ru.common.medicalprocessor.core.service;

import ru.common.medicalprocessor.core.model.entity.MedicalHistory;

public interface MedicalHistoryService extends CrudService<MedicalHistory> {

}
