package ru.common.medicalprocessor.core.service.impl;

import org.springframework.stereotype.Service;
import ru.common.medicalprocessor.core.model.entity.PatientEntity;
import ru.common.medicalprocessor.core.repository.PatientRepository;
import ru.common.medicalprocessor.core.service.PatientService;

import java.util.List;
import java.util.UUID;

@Service
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public PatientEntity findById(UUID id) {
        return patientRepository.findById(id)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public List<PatientEntity> findAll() {
        return patientRepository.findAll();
    }

    @Override
    public PatientEntity save(PatientEntity entity) {
        return patientRepository.save(entity);
    }

    @Override
    public PatientEntity deleteById(UUID id) {
        PatientEntity entity = findById(id);
        patientRepository.deleteById(id);
        return entity;
    }
}
